﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Xmq.Xq.Try
{
    class XQ
    {
    }

    //此类用于思迅商锐9及9.5商品查询
    public static class iss9_select
    {
        public static string Seach(string id)
        {
            string sql = "SELECT item_no, item_name, unit_no,item_clsno, price, sale_price FROM t_bd_item_info WHERE item_no='" + id + "' or item_subno='" + id + "'";
            DataTable dataTable = DbTable(sql);

            //声明[单位][价格][结果]字符串
            string unit, price, result = "没有该商品信息→有可能一品多码"; 
            //判断数据结果集是否为空
            if (dataTable!=null)
            {
                string sql_sale = "SELECT [item_no],[old_price],[spe_price],[sold_qty] ,[start_date],[end_date] FROM[hbposev9].[dbo].[t_rm_spec_price]  where item_no = '" + id + "' order by [end_date] desc";
                //定义da数据库促销价格查询结果集
                DataTable da = DbTable(sql_sale);
                //判断查询结果是否大于零
                if (dataTable.Rows.Count > 0)
                {
                    unit = dataTable.Rows[0][2].ToString().Trim();
                    if (da.Rows.Count > 0 && (Time_Yes(Convert.ToDateTime(da.Rows[0][5]).ToShortDateString().ToString(), DateTime.Now.ToString("yyyy-MM-dd"))))
                    {
                        price = da.Rows[0][2].ToString();
                        //判断是否为生鲜价格单位为KG进行转换 斤
                        if (unit == "kg" || unit == "Kg" || unit == "KG")
                        {
                            result = dataTable.Rows[0][1].ToString() + "→" + (Convert.ToDouble(price) / 2).ToString() + "→" + "斤";
                        }
                        else
                        {
                            result = dataTable.Rows[0][1].ToString() + "→" + da.Rows[0][2].ToString() + "→" + dataTable.Rows[0][2].ToString();
                        }

                    }
                    else
                    {
                        price = dataTable.Rows[0][5].ToString();
                        //判断是否为生鲜价格单位为KG进行转换 斤
                        if (unit == "kg" || unit == "Kg" || unit == "KG")
                        {
                            result = dataTable.Rows[0][1].ToString() + "→" + (Convert.ToDouble(price) / 2).ToString() + "→" + "斤";
                        }
                        else
                        {
                            result = dataTable.Rows[0][1].ToString() + "→" + price + "→" + dataTable.Rows[0][2].ToString();
                        }
                    }

                }
            }
            else
            {
                result = "警告！！！→请检查数据库连接是否正确";
            }
            return result;
        }
        //根据SQL语句获得一个DataTable数据表
        public static DataTable DbTable(string SqlTxt)
        {
            SqlConnection conn=null;
            DataTable dataTable = null;
            SqlDataAdapter sqlDataAdapter;
            string conn_str;
            if (File.Exists("xgm.db"))
            {
                try
                {
                    conn_str = File.ReadAllText("xgm.db");
                    conn = new SqlConnection(conn_str);
                    sqlDataAdapter = new SqlDataAdapter(SqlTxt, conn);
                    //定义dataTable数据库查询结果集
                    dataTable = new DataTable();
                    sqlDataAdapter.Fill(dataTable);
                }
                catch (Exception)
                {
                    return dataTable;
                }
                
                
                return dataTable;
            }
            return dataTable;

        }

        //确定时间确定促销价格
        public static bool Time_Yes(string date, string now_date)
        {
            string[] time = date.Split(new string[] { "-" }, StringSplitOptions.None);
            string[] now_time = now_date.Split(new string[] { "-" }, StringSplitOptions.None);
            if (Convert.ToInt32(time[0]) > Convert.ToInt32(now_time[0]))
            {
                return true;
            }
            else
            {
                if (Convert.ToInt32(time[0]) == Convert.ToInt32(now_time[0]))
                {
                    if (Convert.ToInt32(time[1]) > Convert.ToInt32(now_time[1]))
                    {
                        return true;
                    }
                    else
                    {
                        if (Convert.ToInt32(time[1]) == Convert.ToInt32(now_time[1]))
                        {
                            if (Convert.ToInt32(time[2]) > Convert.ToInt32(now_time[2]))
                            {
                                return true;
                            }
                            else
                            {
                                if (Convert.ToInt32(time[2]) == Convert.ToInt32(now_time[2]))
                                {
                                    return true;
                                }
                                else
                                {
                                    return false;
                                }
                            }
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
                else
                {
                    return false;
                }
            }

        }
    }

    //此类通过socket服务接收数据查询并返回商品
    public class Socket_Server
    {
        private Socket socket;
        public Socket_Server()
        {
            socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        }

        public void Start()
        {
            socket.Bind(new IPEndPoint(IPAddress.Any, 9500));
            socket.Listen(10);
            Console.WriteLine("服务器已启动");
            accept();
            Console.ReadKey();
        }

        private void accept()
        {
            while (true)
            {
                Socket soc1 = socket.Accept();
                IPEndPoint point = soc1.RemoteEndPoint as IPEndPoint;
                Console.WriteLine(point.Address + ":[" + point.Port + "]连接成功");

                Thread thread = new Thread(recive);
                thread.IsBackground = true;
                thread.Start(soc1);
            }

        }

        private void recive(object obj)
        {
            Socket socket = obj as Socket;
            try
            {
                while (true)
                {
                    byte[] data = new byte[1024];
                    //服务端接收信息
                    int num = socket.Receive(data);
                    string result = Encoding.UTF8.GetString(data, 0, num);
                    string da = iss9_select.Seach(result);
                    socket.Send(Encoding.UTF8.GetBytes(da));
                }
            }
            catch (Exception)
            {
                IPEndPoint iPEndPoint = socket.RemoteEndPoint as IPEndPoint;
                Console.WriteLine(iPEndPoint.Address + "对方已断开");
            }

        }
    }
}
